import 'package:app_mail/screens/detail_screen.dart';
import 'package:flutter/material.dart';
import 'package:app_mail/model/backend.dart';
import 'package:app_mail/model/email_model.dart';
import 'package:app_mail/widgets/email_widget.dart';

class ListScreen extends StatefulWidget {
  const ListScreen({Key? key}) : super(key: key);

  @override
  State<ListScreen> createState() => _ListScreenState();
}

//---------------------------------------------------------------
class _ListScreenState extends State<ListScreen> {
  void longPress(Email email) {
    setState(() {
      Backend().markEmailAsRead(email.id);
    });
  }

  void swipe(Email email) {
    setState(() {
      Backend().deleteEmail(email.id);
    });
  }

  void ontap(Email email, context) {
    setState(() {
      Backend().markEmailAsRead(email.id);
    });
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => DetailScreen(email: email)),
    );
  }

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: Backend()
          .getEmails()
          .map((email) => EmailWidget(
                email: email,
                longPress: longPress,
                swipe: swipe,
                ontap: ontap,
              ))
          .toList(),
    );
  }
}
